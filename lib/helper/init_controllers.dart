import 'package:get/get.dart';
import 'package:get_x_demo/controllers/list_controller.dart';
import 'package:get_x_demo/controllers/tap_controller.dart';

Future<void> init() async {
  Get.put<ListController>(ListController());
  // Get.put<TapController>(TapController());
}
