import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get_x_demo/controllers/list_controller.dart';
import 'package:get_x_demo/controllers/tap_controller.dart';
import 'package:get_x_demo/my_home_page.dart';

class ThirdPage extends StatelessWidget {
  const ThirdPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TapController controller = Get.find<TapController>();
    // TapController controller = Get.put<TapController>(TapController());
    ListController listController = Get.find();
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              )),
          title: Text('ThirdPage')),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          // width: double.maxFinite,
          // height: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(() => Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.all(20),
                        height: 100,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0xFF89dad0)),
                        child: Center(
                            child: Text(
                          'Total Value: ${Get.find<TapController>().z.value}',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        )),
                      ),
                      Container(
                        margin: const EdgeInsets.all(20),
                        height: 100,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0xFF89dad0)),
                        child: Center(
                            child: Text(
                          'Y Value: ${Get.find<TapController>().y.value}',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        )),
                      ),
                    ],
                  )),
              GestureDetector(
                onTap: () {
                  // controller.increaseX();
                  Get.to(() => MyHomePage());
                },
                child: Container(
                  margin: const EdgeInsets.all(20),
                  height: 100,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFF89dad0)),
                  child: Center(
                      child: Text(
                    'X value: ${Get.find<TapController>().x.toString()}',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  )),
                ),
              ),
              GestureDetector(
                onTap: () {
                  // controller.increaseX();
                  Get.find<TapController>().increaseY();
                },
                child: Container(
                  margin: const EdgeInsets.all(20),
                  height: 100,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFF89dad0)),
                  child: Center(
                      child: Text(
                    'Increase Y',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  )),
                ),
              ),
              GestureDetector(
                onTap: () {
                  // controller.increaseX();
                  Get.find<TapController>().totalXY();
                },
                child: Container(
                  margin: const EdgeInsets.all(20),
                  height: 100,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFF89dad0)),
                  child: Center(
                      child: Text(
                    'Total X+Y',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  )),
                ),
              ),
              GestureDetector(
                onTap: () {
                  // controller.increaseX();
                  Get.find<ListController>()
                      .setValues(Get.find<TapController>().z.value);
                },
                child: Container(
                  margin: const EdgeInsets.all(20),
                  height: 100,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xFF89dad0)),
                  child: Center(
                      child: Text(
                    'Save total',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
